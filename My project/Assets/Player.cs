using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody2D rb;
    float speed = 7f, jump = 9f;
    bool ground;

    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Space) && ground)
        {
            rb.velocity = new Vector2(rb.velocity.x, jump);
            animator.SetBool("IsJumping", true);
        }
        float moveX = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveX*speed, rb.velocity.y);

        animator.SetFloat("Speed", Mathf.Abs(moveX));
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "ground")
        {
            ground = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            ground = false;
        }
    }
    void Update()
    {

    }
}
