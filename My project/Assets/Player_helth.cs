using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player_helth : MonoBehaviour
{
    [SerializeField] int health = 100;
    [SerializeField] int maxhealth = 100;
    [SerializeField] Slider slider;

    public void TakeDamage(int health)
    {
        this.health -= health;
        if (this.health < 0)
        {
            Die();
        }
        UpdateHealthBar();
    }

    private void Die()
    {
        SceneManager.LoadScene(0);
    }

    private void UpdateHealthBar()
    {
        slider.value = this.health;
    }

    void Start()
    {
        slider.minValue = 0;
        slider.maxValue = maxhealth;
        slider.value = health;
    }
}
