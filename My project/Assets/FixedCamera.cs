using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedCamera : MonoBehaviour
{
    [SerializeField] Transform playerPosition;
    void Update()
    {
        Vector3 newCameraPosition = new Vector3(playerPosition.position.x, 0.23f, -10);
        gameObject.transform.position = newCameraPosition;
    }
}
