using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour
{
    [SerializeField] int attackDamage = 5;
    [SerializeField] float attackRange = 5f;
    [SerializeField] float nextAttackTime = 0;
    void OnTriggerEnter2D(Collider2D other)
    {
        Damage(other);
    }
    void OnTriggerStay2D(Collider2D other)
    {
        Damage(other);
    }
    void Damage(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (nextAttackTime < Time.time)
            {
                other.gameObject.GetComponent<Player_helth>().TakeDamage(attackDamage);
                nextAttackTime = Time.time + 5f / 2f;
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, attackRange);
    }

}
